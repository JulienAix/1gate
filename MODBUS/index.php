<html xmlns = "http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

	<head>
 		<title>1gate configurator</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link href="style.css?v3" rel="stylesheet" type="text/css"/>
	</head>

	<body>

        <div class="header">
        
		<p><a href="index.php"><img src="img/atim.png" alt="ATIM"></a></p>

		<h1>1gate configurator</h1>

		<div class ="version">
			<br />
			<?php
				//Modbus srevice version
                        	$modbusVersion = file_get_contents('/var/www/html/modbus.version');
                        	echo "Modbus service version $modbusVersion";
                        	echo("<br /><br />");

                        	//Chirpstack Version
                        	$chirpstackVersion = shell_exec('chirpstack-gateway-bridge version');
                        	echo "Chirpstack version $chirpstackVersion";

			?>
		</div>

        </div>

	<ul class = "menu">

 		<li><a href="#menu1">LAN</a></li>
		<li class = "separator"></li>
  		<li></div><a href="#menu2">MQTT</a></li>
		<li class = "separator"></li>
  		<li><a href="#menu4">MODBUS</a></li>
	</ul>

	<br />

                <!--------------------------------------------------------------------------------------------------------------------------------->
                <!--MENU 0 : START PAGE------- ---------------------------------------------------------------------------------------------------->
                <!--------------------------------------------------------------------------------------------------------------------------------->
                <div id="menu0">
                </div>


	<div class="body">


		<!--------------------------------------------------------------------------------------------------------------------------------->
                <!--MENU 1 : LAN CONFIGURATION ---------------------------------------------------------------------------------------------------->
		<!--------------------------------------------------------------------------------------------------------------------------------->

		<div id="menu1">
		<form method="post" action="process1.php" enctype="multipart/form-data">
		<?php		       
		//1. Check Ethernet connection

		//1.0 Get hostname
                $host=shell_exec('hostname');


		//1.1 Check if ethernet peripheral is active
		$content=shell_exec('ifconfig eth0');
                //Parse lines
		$lines = explode("\n", $content);
		//echo($lines[1]);
                
		//Get ethernet IP address
                $ips = preg_split('/\s+/', $lines[1]);

		//1.2 Check if static adressing is active
                //Files path to be checked
                $dhcpFile='/etc/network/interfaces';

               	//Open the files to be checked( connection status and configuration)
              	$dhcpContent = file_get_contents($dhcpFile);

               	//parse the lines 
                $dhcpLines = explode("\n", $dhcpContent);
		
                if(strcmp($dhcpLines[11],"allow-hotplug eth0") == 0) $ethernet = 0;

		elseif(strpos($dhcpLines[12],"dhcp") != false)$ethernet = 1;

                elseif(strpos($dhcpLines[12],"static") != false)$ethernet = 2;
			
        	?>
		<h2>Ethernet :</h2>
                <?php

		//If ethernet connection is active display current IP address
                
                echo ("Current IP : <b> $ips[2] </b>");
                echo("<br /><br />");
                echo ("Hostname : <b> $host </b>");
                echo("<br /><br />");

		//Get information from configuration file

                $displayIP = '';
                $displayMask = '';
                $displayGW = '';

                if($ethernet == 2)
                {

                //Extract IP static address
                $displayIP = substr($dhcpLines[13],12);
                //echo($displayIP);

                $displayMask = substr($dhcpLines[14],12);
                //echo($displayMask);

                $displayGW = substr($dhcpLines[15],12);
                //echo($displayGW);

                ?>

                	<label class="container"> DHCP
                        	<input type="radio" name="ethernet" value ="dhcp">
                                <span class="checkmark"></span>
                         </label>

                        <label class="container"> STATIC
                        	<input type="radio" name="ethernet" value ="static" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> CONFIGURATION
                                <input type="radio" name="ethernet" value ="rooter">
                                <span class="checkmark"></span>
                        </label>


                <br /><br /><br />

		<?php
                }

                elseif ($ethernet == 1)
                {
                ?>

                        <label class="container"> DHCP
                                <input type="radio" name="ethernet" value ="dhcp" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> STATIC
                                <input type="radio" name="ethernet" value ="static">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> CONFIGURATION
                                <input type="radio" name="ethernet" value ="rooter">
                                <span class="checkmark"></span>
                        </label>


                <br /><br /><br />
                <?php
                }

                elseif ($ethernet == 0)
                {
                ?>

                        <label class="container"> DHCP
                                <input type="radio" name="ethernet" value ="dhcp">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> STATIC
                                <input type="radio" name="ethernet" value ="static">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> CONFIGURATION
                                <input type="radio" name="ethernet" value ="rooter" checked="checked">
                                <span class="checkmark"></span>
                        </label>


                <br /><br /><br />
                <?php
                }
                ?>

		<label for="ethIP">Static IP address:</label><br>
                <label class="text_field">
                	<input type="text" id="ethIP"  name="ethIP" size="12" value="<?php echo $displayIP; ?>"/>
                </label>
                <br /><br />

                <label for="ethMASK">Netmask:</label><br>
                <label class="text_field">
                        <input type="text" id="ethMASK"  name="ethMASK" size="12"  value="<?php echo $displayMask; ?>"/>
                </label>
                <br /><br />

		<label for="ethGW">Gateway:</label><br>
                <label class="text_field">
                	<input type="text" id="ethGW"  name="ethGW"size="12"  value="<?php echo $displayGW; ?>"/>
                </label>
                <br /><br />

                <h2>Wifi hotspot :</h2>

		<?php


		//2. Wifi hotspot
                //2.1 Check if wifi peripheral is active
                $content=shell_exec('cat /sys/class/net/wlan0/operstate');
                //echo(strcmp($content,"up\n"));
		//echo(strcmp($content,"down\n"));
		if(strcmp($content, "down\n") == 0)$hotspot = 0;
                else if(strcmp($content,"up\n") == 0)$hotspot = 1;
		//echo($hotspot);

		if($hotspot == 1)
                {
                ?>

                        <label class="container"> ON
                                <input type="radio" name="hotspot" value ="on" checked="checked">
                                <span class="checkmark"></span>
                         </label>

                        <label class="container"> OFF
                                <input type="radio" name="hotspot" value ="off" >
                                <span class="checkmark"></span>
                        </label>

                <br /><br />

                <?php
                }

                else if($hotspot == 0)
                {
                ?>

                        <label class="container"> ON
                                <input type="radio" name="hotspot" value ="on">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="hotspot" value ="off" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                <br /><br />

                <?php

		}

		//3.NTP Time synchronisation
                
                //Files path to be checked
                $ntpFile='/etc/systemd/timesyncd.conf';

                //Open the files to be checked( connection status and configuration)
                $ntpContent = file_get_contents($ntpFile);

                //parse the lines 
                $ntpLines = explode("\n", $ntpContent);

                ?>
                <h2>NTP server :</h2>
                <?php

		//echo($ntpLines[14]);

		if(strpos($ntpLines[14],"NTP=") == 1)$ntp = 0;//If "NTP" is in position 1 -> #NTP= is written -> NTP is OFF

                else if(strpos($ntpLines[14],"NTP=") == 0)//If "NTP" is in position 0 -> NTP=.... is written -> NTP is OFF
		{
			$ntp = 1;
			//Get the NTP address
			$displayNTP = substr($ntpLines[14],4);
		}

		//echo($ntp);

		if($ntp == 1)
                {
                ?>

                        <label class="container"> ON
                                <input type="radio" name="ntp" value ="on" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="ntp" value ="off" >
                                <span class="checkmark"></span>
                        </label>

			<br /><br />

                	<label for="ntpIP">IP address:</label><br>
                	<label class="text_field">
                        	<input type="text" id="ntpIP"  name="ntpIP" size="12" value="<?php echo $displayNTP; ?>"/>
                	</label>
                
                	<br /><br />

                <?php
                }

                else if($ntp == 0)
                {

                ?>

                        <label class="container"> ON
                                <input type="radio" name="ntp" value = "on">
                                <span class="checkmark"></span>
                         </label>

                        <label class="container"> OFF
                                <input type="radio" name="ntp" value = "off"  checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <br /><br />

                        <label for="ntpIP">IP address:</label><br>
                        <label class="text_field">
                                <input type="text" id="ntpIP"  name="ntpIP" size="12">
                        </label>

	                <br /><br />

		<?php
		}


		/*
		//2.Check wifi connection
	        //2.1 Check if wifi peripheral is active
                $content=shell_exec('ifconfig wlan0');
                //Parse lines
                $lines = explode("\n", $content);
                //echo($lines[1]);
                if(strpos($lines[1],"inet") != false)
                {
	                $wifi = 1;
	                //echo("test1");
                }
                else $wifi = 0;

                //Get IP address
                $ips = preg_split('/\s+/', $lines[1]);
                //echo "IP address :$ips[2]";

                //2.2 Check if static adressing is active
                //Files path to be checked
                $dhcpFile='/etc/dhcpcd.conf';

                //Open the files to be checked( connection status and configuration)
                $dhcpContent = file_get_contents($dhcpFile);

                //parse the lines
                $dhcpLines = explode("\n", $dhcpContent);
                //echo($dhcpLines[40]);
                if(strpos($dhcpLines[40],"wlan0") != false)
                {
                	$staticWifi = 1;
                        //echo("test2");
		}
		else $staticWifi = 0;
                ?>
                
                <h2>WIFI :</h2>
                <?php

                if($wifi == 1)
                {
	                echo ("Current IP :<b> $ips[2] </b>");
                        echo("<br /><br />");
                        echo ("Hostname : <b> $host </b>");
                        echo("<br /><br />");

                }

                //Display WIFI button according to configuration 
                if($wifi == 0)
                {
                ?>

                	<label class="container"> DHCP
                        	<input type="radio" name="wifi" value ="dhcp">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> STATIC
                        	<input type="radio" name="wifi" value ="static">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="wifi" value ="off" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <br /><br />
                <?php
                }

                else if($wifi == 1 && $staticWifi == 1)
                {
                ?>

                                <label class="container"> DHCP
                                        <input type="radio" name="wifi" value ="dhcp">
                                        <span class="checkmark"></span>
                                </label>

                                <label class="container"> STATIC
                                        <input type="radio" name="wifi" value ="static" checked="checked">
                                        <span class="checkmark"></span>
                                </label>

                                <label class="container"> OFF
                                        <input type="radio" name="wifi" value ="off">
                                        <span class="checkmark"></span>
                                </label>

                                <br /><br />
                <?php
                }

                else if($wifi == 1 && $staticWifi == 0)
                {
                ?>

                                <label class="container"> DHCP
                                        <input type="radio" name="wifi" value ="dhcp" checked="checked">
                                        <span class="checkmark"></span>
                                </label>

                                <label class="container"> STATIC
                                        <input type="radio" name="wifi" value ="static">
                                        <span class="checkmark"></span>
                                </label>

                                <label class="container"> OFF
                                        <input type="radio" name="wifi" value ="off">
                                        <span class="checkmark"></span>
                                </label>

                                <br /><br />
                <?php
                }

                ?>
                        <label for="wlanIP">Static IP address:</label><br>
                        <label class="text_field">
                        	<input type="text" id="wlanIP"  name="wlanIP"size="12"/>
                                / 
                                <input type="text" id="wlanMASK"  name="wlanMASK" size="1"/>

                        </label>
			<br /><br />

                        <label for="wlanGW">Gateway IP address:</label><br>
                        <label class="text_field">
                                <input type="text" id="wlanGW"  name="wlanGW"size="12"/>
                        </label>
                        <br /><br />


			<label>Register WIFI network :</label><br>

			<label class="indication"> SSID </label>
			<label class="text_field">
                        	<input type="text" name="ssid"size="20"/>
                        </label>
			<br />

                        <label class="indication"> Password </label> 
                        <label class="text_field">
	                        <input type="password" name="password"size="20"/>
                        </label>

			<br /><br />
			
		<?php 
		*/
		?>
		<input type="submit" value="OK"/>
        	<br />

		</form>     

		</div> <!-- End of menu1-->

		<!--------------------------------------------------------------------------------------------------------------------------------->
                <!--MENU 2 : MQTT CONFIGURATION ------------------------------------------------------------------------------------------------->
		<!--------------------------------------------------------------------------------------------------------------------------------->

		<div id="menu2">

		<?php
                //Check if modbus service is active
                $content=shell_exec('systemctl status modbus');
                //Parse lines
                $lines = explode("\n", $content);
                //echo($lines[1]);
                if(strpos($lines[2],"running") != false)
                {
                        $modbus = 1;
                }
                else $modbus = 0;

		////////////////////////////////////////////////////////////////////////////////////////////Open MQTT file to load configuration

                //File path
                $chirpstackFile='/etc/chirpstack-application-server/chirpstack-application-server.toml';

                //Open the file
                $chirpstackContent = file_get_contents($chirpstackFile);

                //Parse the lines
                $chirpstackLines = explode("\n", $chirpstackContent);
                
		//Extract URL, PORT and TLS
		preg_match('/"([^"]+)"/', $chirpstackLines[86], $result);//Extarct string between quotes
		$tmpStr = $result[1];
		if(strcmp(substr($tmpStr,0,3), "ssl") == 0) $tls = 1;//Check if ssl or tcp
		else $tls = 0;
		$tmpStr = substr($tmpStr,6);//Extract URL and port
		$tmpArr = explode(":", $tmpStr);
		$displayURL = $tmpArr[0];
		$displayPort = $tmpArr[1];
		
		//Extract username
		preg_match('/"([^"]+)"/', $chirpstackLines[87], $result);//Extarct string between quotes
                $displayUsername = $result[1];

                //Extract password
                preg_match('/"([^"]+)"/', $chirpstackLines[88], $result);//Extarct string between quotes
                $displayPassword = $result[1];
		
                //Extract client id
                preg_match('/"([^"]+)"/', $chirpstackLines[92], $result);//Extarct string between quotes
                $displayClientId = $result[1];

                //Extract event
                preg_match('/"([^"]+)"/', $chirpstackLines[83], $result);//Extarct string between quotes
                $displayEvent = $result[1];

                //Extract command
                preg_match('/"([^"]+)"/', $chirpstackLines[84], $result);//Extarct string between quotes
                $displayCommand = $result[1];

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		?>
		
		<form method="post" action="process2.php" enctype="multipart/form-data">

                <label class="toggleLabel">MQTT integration:</label>

		<?php 
				
		if($modbus == 0)//MQTT integration is on -> display the ids saved in file
                {
                ?>

                       	<label class="container"> ON
                               	<input type="radio" name="broker" value ="extern" checked="checked">
                               	<span class="checkmark"></span>
                       	</label>

                       	<label class="container"> OFF
                               	<input type="radio" name="broker" value ="local">
                               	<span class="checkmark"></span>
                       	</label>
	
                       	<br /><br /><br />

                        <label for="url">URL & port:</label><br>
                        <label class="text_field">
                                <input type="text" id="url"  name="url" size="40" value="<?php echo $displayURL; ?>"/>
                                :
                                <input type="text" id="port"  name="port" size="2" value="<?php echo $displayPort; ?>"/>
                        </label>
                        <br /><br />

                        <label for="username">Username:</label><br>
                        <label class="text_field">
                                <input type="text" id="username"  name="username" size="50" value="<?php echo $displayUsername; ?>"/>
                        </label>
                        <br /><br />

                        <label for="password">Password:</label><br>
                        <label class="text_field">
                                <input type="password" id="password"  name="password" size="50" value="<?php echo $displayPassword; ?>"/>
                        </label>
                        <br /><br />

                        <label for="clientid">Client ID:</label><br>
                        <label class="text_field">
                                <input type="text" id="clientid"  name="clientid" size="50" value="<?php echo $displayClientId; ?>"/>
                        </label>
                        <br /><br />

                        <label for="event">Event topic:</label><br>
                        <label class="text_field">
                                <input type="text" id="event"  name="event" size="50" value="<?php echo $displayEvent; ?>"/>
                        </label>
                        <br /><br />

                        <label for="command">Command topic:</label><br>
                        <label class="text_field">
                                <input type="text" id="command"  name="command" size="50" value="<?php echo $displayCommand; ?>"/>
                        </label>
                        <br /><br /><br />

	                <label class="toggleLabel">TLS secured connection:</label>

			<?php
                	if($tls == 1)
                	{
                	?>

                        	<label class="container"> ON
                                	<input type="radio" name="tls" value ="on" checked="checked">
                                	<span class="checkmark"></span>
                        	</label>

                        	<label class="container"> OFF
                                	<input type="radio" name="tls" value ="off">
                                	<span class="checkmark"></span>
                        	</label>
			<?php
			}
			else
			{
			?>

                                <label class="container"> ON
                                        <input type="radio" name="tls" value ="on">
                                        <span class="checkmark"></span>
                                </label>

                                <label class="container"> OFF
                                        <input type="radio" name="tls" value ="off" checked="checked">
                                        <span class="checkmark"></span>
                                </label>
			<?php
			}
			?>

			<br /><br /><br />

                        <label for="caCert">CA certificate:</label><br>
                        <label class="text_field">
                                <input type="file" id="caCert" name="caCert" size="12" />
                        </label>
                        </label>
                        <br /><br />
                        <label for="clientCert">Client certificate:</label><br>
                        <label class="text_field">
                                <input type="file" id="clientCert" name="clientCert" size="12" />
                        </label>
                        </label>
                        <br /><br />

                        <label for="clientKey">Client Key:</label><br>
                        <label class="text_field">
                                <input type="file" id="clientKey" name="clientKey" size="12" />
                        </label>
                        </label>
                        <br /><br />



                <?php
                }

		else if($modbus == 1)//MQTT integration is off -> empty fields
                {
                ?>

                        <label class="container"> ON
                                <input type="radio" name="broker" value ="extern">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="broker" value ="local" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <br /><br /><br />
        
                        <label for="url">URL & port:</label><br>
                        <label class="text_field">
                                <input type="text" id="url"  name="url"size="40"/>
				:
				<input type="text" id="port"  name="port"size="2"/>
                        </label>
                        <br /><br />

                        <label for="username">Username:</label><br>
                        <label class="text_field">
                                <input type="text" id="username"  name="username" size="50"/>
                        </label>
                        <br /><br />

                        <label for="password">Password:</label><br>
                        <label class="text_field">
                                <input type="password" id="password"  name="password" size="50"/>
                        </label>
                        <br /><br />

                        <label for="clientid">Client ID:</label><br>
                        <label class="text_field">
                                <input type="text" id="clientid"  name="clientid" size="50"/>
                        </label>
                        <br /><br />

                        <label for="event">Event topic:</label><br>
                        <label class="text_field">
                                <input type="text" id="event"  name="event" size="50"/>
                        </label>
                        <br /><br />

                        <label for="command">Command topic:</label><br>
                        <label class="text_field">
                                <input type="text" id="command"  name="command" size="50"/>
                        </label>
                        <br /><br /><br />


                        <label class="toggleLabel">TLS secured connection:</label>


                        <label class="container"> ON
                                <input type="radio" name="tls" value ="on" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="tls" value ="off">
                                <span class="checkmark"></span>
                        </label>
                        
			<br /><br /><br />

                        <label for="caCert">CA certificate:</label><br>
                        <label class="text_field">
                                <input type="file" id="caCert" name="caCert" size="12" />
                        </label>
                        </label>
                        <br /><br />

                        <label for="clientCert">Client certificate:</label><br>
                        <label class="text_field">
                                <input type="file" id="clientCert" name="clientCert" size="12" />
                        </label>
                        </label>
                        <br /><br />

                        <label for="clientKey">Client Key:</label><br>
                        <label class="text_field">
                                <input type="file" id="clientKey" name="clientKey" size="12" />
                        </label>
                        </label>
                        <br /><br />
             
		<?php
                }
                ?>
		
		<input type="submit" value="OK"/>
               	<br />

               	</form>     

                <?php
		//Delate previous logs
                shell_exec("rm /home/ogate/API/chirpstack.log");
                shell_exec("rm /var/www/html/chirpstack.log");


		//Extract logs from journalctl
		$logs = shell_exec("sudo journalctl -u chirpstack-application-server --since yesterday > /home/ogate/API/chirpstack.log");
		
		//Copy the log file to current directory
                shell_exec("sudo mv /home/ogate/API/chirpstack.log /var/www/html");
	
                ?>
               
                <a href="chirpstack.log">
                Download logs
                </a>
		<br/><br/>

		</div> <!-- End of menu2-->

                <!--------------------------------------------------------------------------------------------------------------------------------->
		<!--MENU 4 : MODBUS CONFIGURATION ------------------------------------------------------------------------------------------------->
		<!--------------------------------------------------------------------------------------------------------------------------------->


		<div id="menu4">

		<?php
                //4.1 DISPLAY THE DEVICES REGISTERED

		//Files path to be checked
		$devicesFile='/home/ogate/MODBUS/modbus.dev';
			
		//Open the files to be checked( connection status and configuration)
		$devicesContent = file_get_contents($devicesFile);
			
		//parse the files
		$devicesLines = explode("\n", $devicesContent);
		$devicesCnt = count($devicesLines);
			
                //MODBUS adress start from 0
                $adr = 0;

		if($devicesCnt > 1)
		{
                        //Display MODBUS devices address tab
                        for ($i = 0; $i < $devicesCnt-1; $i++)
			{
				//Parse the line
				$devicesElements = explode(",", $devicesLines[$i]);
				//Split the bytes from MODBUS payload
				$devicesBytes = str_split($devicesElements[1],4);
				//Print devEUI
				?> <strong> devEUI:</strong><?php
				print $devicesElements[0];
			
				//Split time stamp
				//$devicesTime = str_split($devicesElements[2],1);
			
				?> <strong>  Last seen: </strong><?php
				//print "$devicesTime[6]$devicesTime[7]/$devicesTime[4]$devicesTime[5]/$devicesTime[0]$devicesTime[1]$devicesTime[2]$devicesTime[3] $devicesTime[8]$devicesTime[9]:$devicesTime[10]$devicesTime[11]:$devicesTime[12]$devicesTime[13]";
				print $devicesElements[2];
				?>      
				<br /><br />

				<table>
					<tr>
                                                <?php
						//
                                         	for ($j = 0; $j < 25; $j++) 
                                                {
                                                ?>
                                                        <td>
                                                        <?php 
							print $adr;
							?> 
                                                        </td>
                                                        <?php
                                                $adr = $adr + 1;
						}
                                                ?>
					</tr>

					<tr>
                                                <?php
                                                for ($j = 0; $j < 25; $j++) 
                                                {
                                                
                                                ?>
                                                        <td>
                                                        <?php print $devicesBytes[$j];?> 
                                                        </td>
                                                        <?php
                                                }
                                                ?>

					</tr>

				</table>
				<br />

                                <table>
                                        <tr>
                                                <?php
                                                //
                                                for ($j = 25; $j < 50; $j++) 
                                                {
                                                ?>
                                                        <td>
                                                        <?php 
                                                        print $adr;
                                                        ?> 
                                                        </td>
                                                        <?php
                                                $adr = $adr + 1;
						}
                                                ?>
                                        </tr>

                                        <tr>
                                                <?php
                                                for ($j = 25; $j < 50; $j++) 
                                                {

                                                ?>
                                                        <td>
                                                        <?php print $devicesBytes[$j];?> 
                                                        </td>
                                                        <?php
                                                }
                                                ?>

                                        </tr>

                                </table>
                                <br />


                        	<?php
                        	}//End of for loop			
			}//End of if

			else 
			{
				echo "--------------------------------------------------------------------------------------<br/>";
				echo "Device table is empty<br/>";
                                echo "--------------------------------------------------------------------------------------<br/>";
			}
			?>

                <form method="post" action="process4.php" enctype="multipart/form-data">

                <label class="toggleLabel">MODBUS integration:</label>

                <?php

                if($modbus == 1)
                {
                ?>
                        <label class="container"> ON
                                <input type="radio" name="modbus" value ="on" checked="checked">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="modbus" value ="off">
                                <span class="checkmark"></span>
                        </label>

                        <br /><br />
 		<?php
                }
                else
                {
                ?>
                        <label class="container"> ON
                                <input type="radio" name="modbus" value ="on">
                                <span class="checkmark"></span>
                        </label>

                        <label class="container"> OFF
                                <input type="radio" name="modbus" value ="off" checked ="checked">
                                <span class="checkmark"></span>
                        </label>

                        <br /><br />
                <?php
                }
                ?>
                <label class="container">   Reset table
                        <input type="checkbox" name="reset" value ="yes">
                        <span class="checkmark"></span>
                </label>
 		<br /><br />


                <input type="submit" value="OK"/>
                <br />

                </form>     

		<?php

                //4.3 LINK TO DOWNLOAD MODBUS APPLICATION LOGS

                //Copy the log file to current directory
                shell_exec("cp /home/ogate/MODBUS/modbus.log /var/www/html");

                ?>
                <a href="modbus.log">
                Download logs
                </a>
                <br /><br />

		</div><!-- End of menu4-->


	</div><!-- End of body-->

        <div class="footer">
        	<p>Need help ?<br/><a href="https://www.atim.com/en/technical-support/"> www.atim.com/en/technical-support</a></p>
        	
	</div>

    </body>
</html>