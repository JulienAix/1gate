<html xmlns = "http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

    <head>
        <title>ATIM gateway API</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link href="style.css?v=4" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div class="header">

            <p><a href="index.php"><img src="img/atim.png" alt="ATIM"></a></p>

            <h1>
            Gateway API configurator
            </h1>

        </div>

	<div class="body">

	<?php
	
	echo "<br \>";

	//1.Check integration status (local/extern)

	//Configuration path
	$chirpstackFile = '/etc/chirpstack-application-server/chirpstack-application-server.toml';

	//Get content
	$chirpstackContent = file_get_contents($chirpstackFile);

	//Parse lines
	$chirpstackLines = explode("\n", $chirpstackContent);

        //Count the lines
        $chirpstackCnt = count($chirpstackLines);

        if($_POST['broker'] == "local")
        {
		echo "MQTT integration is stopped";
		echo "<br \>";

		//Uncomment in Chirpstack configuration file

 		$fp = fopen($chirpstackFile, 'w');
		//Keep line 1 to 76
                for($i = 0; $i < 76; $i++ )
                {
                        fwrite($fp,$chirpstackLines[$i]);
                        fwrite($fp,"\n");
			//echo $chirpstackLines[$i];
                }

		//Uncomment line 77 to line 81
                for($j = 76; $j < 81; $j++ )
                {
			$line = trim($chirpstackLines[$j]);
			//Check if line starts with a #
 			if (substr($line, 0, 1) == "#") 
			{
				//Remove first characters
            			$line = substr($line, 1);
	  		} 

                        //Add a tab
                        $line = "\t" . $line;

                        //Write to file
                        fwrite($fp,$line);
                        fwrite($fp,"\n");
		}
		
		//Empty space
                fwrite($fp,"\n");
		//header
		fwrite($fp,"\t#IoThink MQTT\n");

                //Comment line 84 to line 96 
                for($k = 83; $k < 96; $k++ )
                {
                        $line = trim($chirpstackLines[$k]);
                        //Check if line starts with a #
                        if (substr($line, 0, 1) != "#") 
                        {
                                //Add comment character
                                $line = "#" . $line;
                        } 

                        //Add a tab
                        $line = "\t" . $line;

                        //Write to file
                        fwrite($fp,$line);
                        fwrite($fp,"\n");
                }

		//Leave the rest of the file 
		for($l = 96; $l < $chirpstackCnt; $l++ )
                {
			fwrite($fp,$chirpstackLines[$l]);
                        fwrite($fp,"\n");
                        //echo $chirpstackLines[$i];
		}

                fclose($fp);
 

	}

        else if($_POST['broker'] == "extern")
        {
		//Check if fields are filled
                if (strlen($_POST['url']) && strlen($_POST['username']) && strlen($_POST['clientid']) && strlen($_POST['password']) && strlen($_POST['event']) && strlen($_POST['command'])) 
                {

		//Stop MODBUS service
		shell_exec('sudo systemctl disable modbus');
                shell_exec('sudo systemctl stop modbus');

                echo "MQTT integration is active";
                echo "<br \>";

                //Uncomment in Chirpstack configuration file

                $fp = fopen($chirpstackFile, 'w');
                //Keep line 1 to 76
                for($i = 0; $i < 76; $i++ )
                {
                        fwrite($fp,$chirpstackLines[$i]);
                        fwrite($fp,"\n");
                        //echo $chirpstackLines[$i];
                }

                //Comment line 77 to line 81
                for($j = 76; $j < 81; $j++ )
                {
                        $line = trim($chirpstackLines[$j]);
                        //Check if line starts with a #
                        if (substr($line, 0, 1) != "#") 
                        {
                                //Remove first characters
                                $line = "#" . $line;
                        } 

                        //Add a tab
                        $line = "\t" . $line;

                        //Write to file
                        fwrite($fp,$line);
                        fwrite($fp,"\n");
                }

                //Empty space
                fwrite($fp,"\n");
                //header
                fwrite($fp,"\t#IoThink MQTT\n");

                //Complete file with form
		$line="\tevent_topic_template=\"" . $_POST['event'] . "\"";
		fwrite($fp,$line);
		fwrite($fp,"\n");
                $line="\tcommand_topic_template=\"" . $_POST['command'] . "\"";
                fwrite($fp,$line);
                fwrite($fp,"\n");
                fwrite($fp,"\tretain_events=false");
                fwrite($fp,"\n");
                $line="\tserver=\"ssl://" . $_POST['url'] . ":" . $_POST['port']  . "\"";
                fwrite($fp,$line);
                fwrite($fp,"\n");
                $line="\tusername=\"" . $_POST['username'] . "\"";
                fwrite($fp,$line);
                fwrite($fp,"\n");
                $line="\tpassword=\"" . $_POST['password'] . "\"";
                fwrite($fp,$line);
                fwrite($fp,"\n");
                fwrite($fp,"\tmax_reconnect_interval=\"1m0s\"");
                fwrite($fp,"\n");
                fwrite($fp,"\tqos=1");
                fwrite($fp,"\n");
                fwrite($fp,"\tclean_session=false");
                fwrite($fp,"\n");
                $line="\tclient_id=\"" . $_POST['clientid'] . "\"";
                fwrite($fp,$line);
                fwrite($fp,"\n");
                fwrite($fp,"\tca_cert=\"/home/ogate/API/ca.crt\"");
                fwrite($fp,"\n");
                fwrite($fp,"\ttls_cert=\"/home/ogate/API/client.crt\"");
                fwrite($fp,"\n");
                fwrite($fp,"\ttls_key=\"/home/ogate/API/client.key\"");
                fwrite($fp,"\n");
		fwrite($fp,"\n");

                //Leave the rest of the file 
                for($l = 97; $l < $chirpstackCnt; $l++ )
                {
                        fwrite($fp,$chirpstackLines[$l]);
                        fwrite($fp,"\n");
                }

                fclose($fp);


		//Copy the certificates 

        	//test if CA certificate has been uploaded without errors
        	if(isset($_FILES['caCert']) AND $_FILES['caCert']['error'] == 0)
        	{

                	//Check the type of the file (must be .cer)
                	$file_data = pathinfo($_FILES['caCert']['name']); 
                	$file_type = $file_data['extension'];
                	$extension = array('crt','cer','pem');

                	if(in_array($file_type, $extension))
                	{
                        	//Move the temporary file uploaded to API folder
                        	move_uploaded_file($_FILES['caCert']['tmp_name'], '/home/ogate/API/ca.crt');
                        	echo "CA root certificate has been uploaded";
                        	echo "<br \>";
                        }   
                	else
                	{
                        	echo "Error uploading CA root certificate : wrong extension";
                        	echo "<br \>";
                	}

        	}

                //test if Client certificate has been uploaded without errors
                if(isset($_FILES['clientCert']) AND $_FILES['clientCert']['error'] == 0)
                {

                        //Check the type of the file (must be .cer)
                        $file_data = pathinfo($_FILES['clientCert']['name']); 
                        $file_type = $file_data['extension'];
                        $extension = array('crt','cer','pem');

                        if(in_array($file_type, $extension))
                        {
                                //Move the temporary file uploaded to API folder
                                move_uploaded_file($_FILES['clientCert']['tmp_name'], '/home/ogate/API/client.crt');
                                echo "Client certificate has been uploaded";
                                echo "<br \>";
                        }   
                        else
                        {
                                echo "Error uploading client certificate : wrong extension";
                                echo "<br \>";
                        }

                }

                //test if Client key has been uploaded without errors
                if(isset($_FILES['clientKey']) AND $_FILES['clientKey']['error'] == 0)
                {

                        //Check the type of the file (must be .key)
                        $file_data = pathinfo($_FILES['clientKey']['name']); 
                        $file_type = $file_data['extension'];
                        $extension = array('key');

                        if(in_array($file_type, $extension))
                        {
                                //Move the temporary file uploaded to API folder
                                move_uploaded_file($_FILES['clientKey']['tmp_name'], '/home/ogate/API/client.key');
                                echo "Client key has been uploaded";
                                echo "<br \>";
                        }   
                        else
                        {
                                echo "Error uploading client key : wrong extension";
                                echo "<br \>";
                        }

                }

		//Restart application server
                shell_exec('sudo systemctl restart chirpstack-application-server');
		}//End of if(ALL FIELDS ARE FILLED)

		else 
		{
                	echo "Error all fields must be filled";
                        echo "<br \>";
		}
        }

	?>

	</div>        

        <div class="footer">
            <p>Need help? Visit <a href="https://www.atim.com/en/technical-support/"> www.atim.com/en/technical-support</a></p>
        </div>

    </body>
</html>
