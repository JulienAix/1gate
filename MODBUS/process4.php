
<html xmlns = "http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

    <head>
        <title>ATIM gateway API</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link href="style.css?v=4" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div class="header">

            <p><a href="index.php"><img src="img/atim.png" alt="ATIM"></a></p>

            <h1>
            Gateway API configurator
            </h1>

        </div>

        <div class="body">

        <?php

	#!/bin/su root

	//Configuration path
        $chirpstackFile = '/etc/chirpstack-application-server/chirpstack-application-server.toml';

        //Get content
        $chirpstackContent = file_get_contents($chirpstackFile);

        //Parse lines
        $chirpstackLines = explode("\n", $chirpstackContent);

        //Count the lines
        $chirpstackCnt = count($chirpstackLines);


        $tableFile='/home/ogate/MODBUS/modbus.dev';
        echo "<br \>";


	if ($_POST['reset'] == 'yes')
	{
        	//Clear device list

		$fp = fopen($tableFile, 'w');
		fclose($fp);
        
        	echo "Device table has been cleared";
        	echo "<br \>";

	}
               

        if ($_POST['modbus'] ==  "on")
        {

                //Start MODBUS service
        	shell_exec('sudo systemctl enable modbus');
        	shell_exec('sudo systemctl restart modbus');

                //Uncomment in Chirpstack configuration file
                $fp = fopen($chirpstackFile, 'w');
                //Keep line 1 to 76
                for($i = 0; $i < 76; $i++ )
                {
                        fwrite($fp,$chirpstackLines[$i]);
                        fwrite($fp,"\n");
                        //echo $chirpstackLines[$i];
                }

                //Uncomment line 77 to line 81
                for($j = 76; $j < 81; $j++ )
                {
                        $line = trim($chirpstackLines[$j]);
                        //Check if line starts with a #
                        if (substr($line, 0, 1) == "#") 
                        {
                                //Remove first characters
                                $line = substr($line, 1);
                        } 

                        //Add a tab
                        $line = "\t" . $line;

                        //Write to file
                        fwrite($fp,$line);
                        fwrite($fp,"\n");
                }
                //Empty space
                fwrite($fp,"\n");
                //header
                fwrite($fp,"\t#IoThink MQTT\n");

                //Comment line 84 to line 96 
                for($k = 83; $k < 96; $k++ )
                {
                        $line = trim($chirpstackLines[$k]);
                        //Check if line starts with a #
                        if (substr($line, 0, 1) != "#") 
                        {
                                //Add comment character
                                $line = "#" . $line;
                        } 

                        //Add a tab
                        $line = "\t" . $line;

                        //Write to file
                        fwrite($fp,$line);
                        fwrite($fp,"\n");
                }

                //Leave the rest of the file 
                for($l = 96; $l < $chirpstackCnt; $l++ )
                {
                        fwrite($fp,$chirpstackLines[$l]);
                        fwrite($fp,"\n");
                        //echo $chirpstackLines[$i];
                }

                fclose($fp);

                //Restart application server
                shell_exec('sudo systemctl restart chirpstack-application-server');

		echo "MODBUS integration is active";
                echo "<br \>";
	}

	elseif($_POST['modbus'] ==  "off")
        {
                //Clear device list
                shell_exec('sudo systemctl disable modbus');
                shell_exec('sudo systemctl stop modbus');
                echo "MODBUS integration is stopped";
                echo "<br \>";
        }

        ?>

        </div>        

        <div class="footer">
            <p>Need help? Visit <a href="https://www.atim.com/en/technical-support/"> www.atim.com/en/technical-support</a></p>
        </div>

    </body>
</html>
