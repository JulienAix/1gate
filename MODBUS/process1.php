<html xmlns = "http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

    <head>
        <title>ATIM gateway API</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>

        <div class="header">

            <p><a href="index.php"><img src="img/atim.png" alt="ATIM"></a></p>

            <h1>
            1gate configurator
            </h1>

        </div>

        <div class="body">

        <?php
	        
        echo "<br \>";

        //1.Check the state of ethernet

	//Open configuration file
        $dhcpFile='/etc/network/interfaces';
	//Get content of the file
        $dhcpContent = file_get_contents($dhcpFile);
        //Parse the lines
        $dhcpLines = explode("\n", $dhcpContent);
	//Count the lines
        $dhcpCnt = count($dhcpLines);

        $rcFile='/etc/rc.local';//boot file
        $rcContent = file_get_contents($rcFile);//Parse the lines
        $rcLines = explode("\n", $rcContent);

        if($_POST['ethernet'] == "dhcp")
        {
                echo "Ethernet is in DHCP mode";
                echo "<br \>";

                //Open configuration file
                $fp = fopen($dhcpFile, 'w');

		//Keep first 12 lines
                for($i = 0; $i < 11; $i++ )
                {
                        fwrite($fp,$dhcpLines[$i]);
                        fwrite($fp,"\n");
	        }

		//Ethernet DHCP lines
		fwrite($fp,"auto eth0\n");
		fwrite($fp,"iface eth0 inet dhcp");
                fwrite($fp, "\n");
		fclose($fp);

		//Update hotspot configuration files
                shell_exec('sudo rm /etc/dnsmasq.conf');
                shell_exec('sudo cp /etc/dnsmasq.conf.wlan0 /etc/dnsmasq.conf');

                shell_exec('sudo rm /etc/dhcpcd.conf');
                shell_exec('sudo cp /etc/dhcpcd.conf.wlan0 /etc/dhcpcd.conf');                

                shell_exec('sudo rm /etc/hostapd/hostapd.conf');
                shell_exec('sudo cp /etc/hostapd/hostapd.conf.wlan0 /etc/hostapd/hostapd.conf');

        }

        else if($_POST['ethernet'] == "static")
        {
		//Check if IP adress field has been filled
		if( isset($_POST['ethIP']) && strlen($_POST['ethIP']) && isset($_POST['ethGW']) && strlen($_POST['ethGW'])&& isset($_POST['ethMASK']) && strlen($_POST['ethMASK']) ) 
		{
                        echo "Ethernet is in STATIC mode : ";
                        echo $_POST['ethIP'];
                        echo "<br \>";

	                //Open configuration file
        	        $fp = fopen($dhcpFile, 'w');

                	//Keep first 12 lines
                	for($i = 0; $i < 11; $i++ )
                	{
                       		fwrite($fp,$dhcpLines[$i]);
                       		fwrite($fp,"\n");
                	}

			//Ethernet static lines
                	fwrite($fp,"auto eth0\n");

			//Add the lines for static IP configuration
			fwrite($fp,"iface eth0 inet static\n");
			fwrite($fp,"    address ");
                	fwrite($fp, $_POST['ethIP']);
			fwrite($fp, "\n");
                	fwrite($fp,"    netmask ");
			fwrite($fp, $_POST['ethMASK']);
                	fwrite($fp, "\n");
                	fwrite($fp,"    gateway ");
                	fwrite($fp, $_POST['ethGW']);
			fclose($fp);

                	//Update hotspot configuration files
                	shell_exec('sudo rm /etc/dnsmasq.conf');
                	shell_exec('sudo cp /etc/dnsmasq.conf.wlan0 /etc/dnsmasq.conf');

                	shell_exec('sudo rm /etc/dhcpcd.conf');
                	shell_exec('sudo cp /etc/dhcpcd.conf.wlan0 /etc/dhcpcd.conf');

                	shell_exec('sudo rm /etc/hostapd/hostapd.conf');
                	shell_exec('sudo cp /etc/hostapd/hostapd.conf.wlan0 /etc/hostapd/hostapd.conf');

		}
		else 
		{
			echo "Error : All fields should be filled";
			echo "<br \>";
		}
        }

        else if($_POST['ethernet'] == "rooter")
        {

		echo "Ethernet is in CONFIGURATION mode";
                echo "<br \>";

                //Open configuration file
                $fp = fopen($dhcpFile, 'w');

                //Keep first 12 lines
                for($i = 0; $i < 11; $i++ )
                {
                        fwrite($fp,$dhcpLines[$i]);
                        fwrite($fp,"\n");
                }

                //Ethernet hotspot lines
                fwrite($fp,"allow-hotplug eth0\n");
                fwrite($fp,"iface eth0 inet static\n");
                fwrite($fp,"    address 192.168.1.1\n");
                fwrite($fp,"    netmask 255.255.255.0\n");
                fwrite($fp,"    network 192.168.1.0\n");
                fwrite($fp,"    broadcast 192.168.0.255\n");
		fclose($fp);

                //Open configuration file
                $fp = fopen($rcFile, 'w');
                //Keep fist 20 lines
                for($i = 0; $i < 21; $i++ )
                {
                        fwrite($fp,$rcLines[$i]);
                        fwrite($fp,"\n");
                }
                fwrite($fp,"ifconfig wlan0 down");//Keep the line that turns wifi down at boot
                fwrite($fp,"\n\nexit 0\n");
                fclose($fp);

                echo "Wifi hotspot is OFF";
                echo "<br \>";
                shell_exec('sudo ifconfig wlan0 down');

                //Update hotspot configuration files
                shell_exec('sudo rm /etc/dnsmasq.conf');
                shell_exec('sudo cp /etc/dnsmasq.conf.eth0 /etc/dnsmasq.conf');

                shell_exec('sudo rm /etc/dhcpcd.conf');
                shell_exec('sudo cp /etc/dhcpcd.conf.eth0 /etc/dhcpcd.conf');

                shell_exec('sudo rm /etc/hostapd/hostapd.conf');
                shell_exec('sudo cp /etc/hostapd/hostapd.conf.eth0 /etc/hostapd/hostapd.conf');

	}


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //2.Check the state of wifi hotspot


        if($_POST['hotspot'] == "on" && $_POST['ethernet'] != "rooter")
        {

                //Open configuration file
        	$fp = fopen($dhcpFile, 'a');
	        //Wifi hotspot lines
                fwrite($fp,"\n");
        	fwrite($fp,"allow-hotplug wlan0\n");
		fwrite($fp,"iface wlan0 inet static\n");
                fwrite($fp,"    address 192.168.1.1\n");
                fwrite($fp,"    netmask 255.255.255.0\n");
                fwrite($fp,"    network 192.168.1.0\n");
                fwrite($fp,"    broadcast 192.168.1.255\n");
		fclose($fp);
	
                //Open configuration file
                $fp = fopen($rcFile, 'w');
                //Keep fist 20 lines
                for($i = 0; $i < 21; $i++ )
                {
                        fwrite($fp,$rcLines[$i]);
                        fwrite($fp,"\n");
                }
		fwrite($fp,"#ifconfig wlan0 down");//Comment line that turns wifi down at boot
		fwrite($fp,"\n\nexit 0\n");
                fclose($fp);

		echo "Wifi hotspot is ON";
		echo "<br \>";
		shell_exec('sudo ifconfig wlan0 up');
	}

	else if($_POST['hotspot'] == "off" && $_POST['ethernet'] != "rooter")
        {

                //Open configuration file
                $fp = fopen($rcFile, 'w');
                //Keep fist 20 lines
                for($i = 0; $i < 21; $i++ )
                {
                        fwrite($fp,$rcLines[$i]);
                        fwrite($fp,"\n");
                }
                fwrite($fp,"ifconfig wlan0 down");//Keep the line that turns wifi down at boot
                fwrite($fp,"\n\nexit 0\n");
                fclose($fp);

		echo "Wifi hotspot is OFF";
		echo "<br \>";
                shell_exec('sudo ifconfig wlan0 down');
        }



	////////////////////////////////////////////////////////////////////////////////////////////$
        //3.Check the state of NTP
        
        //Files path to be checked
        $ntpFile='/etc/systemd/timesyncd.conf';

        //Open the files to be checked( connection status and configuration)
        $ntpContent = file_get_contents($ntpFile);

        //parse the lines 
        $ntpLines = explode("\n", $ntpContent);

	//Open configuration file
        $fp = fopen($ntpFile, 'w');

        //Keep first 13 lines
        for($i = 0; $i < 14; $i++ )
        {
               fwrite($fp,$ntpLines[$i]);
               fwrite($fp,"\n");
        }
	if($_POST['ntp'] == "off")
	{
		echo "NTP time synchronization is OFF";
		fwrite($fp,"#NTP=\n");
		fwrite($fp,"#FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org");
	}
        else if($_POST['ntp'] == "on")
        {
		if(isset($_POST['ntpIP']) && strlen($_POST['ntpIP']))
		{
			echo "NTP time synchronization is ON";
                	fwrite($fp,"NTP=");
			fwrite($fp, $_POST['ntpIP']);
                	fwrite($fp, "\n#FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org");
		}
		else 
		{
			echo "NTP IP address is missing";
                	fwrite($fp,"#NTP=\n");
                	fwrite($fp,"#FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org");
        	}
	}
	fclose($fp);


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //2.Check the state of wifi
	/*
        if($_POST['ethernet'] == "static" && $_POST['wifi'] == "static")
        {
                echo "ERROR : WIFI and Ethernet are configured with static address - Priority is given to WIFI";
                echo "<br \>";
        }


        if($_POST['wifi'] == "dhcp")
        {
                echo "WIFI is configured using DHCP";
                echo "<br \>";
                
		if($_POST['ethernet'] != "static")
		{
                	//Delete the last configuration lines
                	$fp = fopen($dhcpFile, 'w');
                	for($i = 0; $i < 40; $i++ )
                	{
                        	fwrite($fp,$dhcpLines[$i]);
                        	fwrite($fp,"\n");
                	}
			fclose($fp);
                }
		shell_exec('sudo rfkill unblock wifi');
		
        }

        elseif($_POST['wifi'] == "static" && $_POST['ethernet'] != "static" )
        {
                //Check if IP address has been filled
                if ( isset($_POST['wlanIP']) && strlen($_POST['wlanIP']) ) 
                {
                        echo "WIFI is configured using static IP : ";
                        echo $_POST['wlanIP'];
                        echo "<br \>";
                }
                else 
                {
                        echo "ERROR : WIFI IP static address is missing ";
                        echo "<br \>";
                }

                //Delete the last configuration lines
                $fp = fopen($dhcpFile, 'w');
                for($i = 0; $i < 40; $i++ )
                {
                        fwrite($fp,$dhcpLines[$i]);
                        fwrite($fp,"\n");
                }
                //Add the lines for static IP configuration
                fwrite($fp,"interface wlan0\n");
                fwrite($fp,"static ip_address=");
                fwrite($fp, $_POST['wlanIP']);
		fwrite($fp, "/");
                fwrite($fp, $_POST['wlanMASK']);
                fwrite($fp, "\nstatic routers=");
                fwrite($fp, $_POST['wlanGW']);
                fclose($fp);

     	        //Turn peripheral ON
                shell_exec('sudo ifconfig wlan0 up');

	}

        elseif($_POST['wifi'] == "off")
        {
                echo "Wifi peripheral is turned OFF";
                echo "<br \>";
                shell_exec('sudo rfkill block wifi');
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //3.Check if a wifi network has been registered

	if( isset($_POST['ssid']) && strlen($_POST['ssid']) )
	{
		//Open wifi network file
        	$wifiFile='/etc/wpa_supplicant/wpa_supplicant.conf';
        	//Get content of the file
        	$wifiContent = file_get_contents($wifiFile);
        	//Parse the lines
        	$wifiLines = explode("\n", $wifiContent);
        	//Count the lines
        	$wifiCnt = count($wifiLines);
		echo "WIFI network ";
                echo($_POST['ssid']);
		echo " is registered";
                echo "<br \>";
		
		$fp = fopen($wifiFile, 'w');
                for($i = 0; $i < $wifiCnt; $i++ )
                {
                        fwrite($fp,$wifiLines[$i]);
			//echo($wifiLines[$i]);
                        fwrite($fp,"\n");
                }

		//Add the new lines
		fwrite($fp, "network={\n    ssid=\"");
		fwrite($fp, $_POST['ssid']);
		fwrite($fp, "\"\n    psk=\"");
		fwrite($fp, $_POST['password']);
		fwrite($fp, "\"\n}");
		fclose($fp);
		
	}

*/
	//Reboot the gateway
        //shell_exec('sudo systemctl restart networking');
	//echo "Gateway needs to be restarted ...";
        
	
	?>
	
	<form method="post" action="restart.php" enctype="multipart/form-data">

	<input type="submit" value="RESTART"/>

	</form>

	</div>

        <div class="footer">
            <p>Need help ? <br /><a href="https://www.atim.com/en/technical-support/"> www.atim.com/en/technical-support</a></p>
        </div>

    </body>
</html>

